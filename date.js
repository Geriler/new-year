﻿var paragraph = document.getElementById("date");

setInterval(function() {
	var date_now = new Date();
	var date_year = date_now.getFullYear() + 1;
	var date_now_timestamp = Date.parse(date_now);
	var new_year = Date.parse(new Date(date_year, 0, 1, 0, 0, 0));
	var seconds = ((new_year - date_now_timestamp) / 1000);
	var left = "осталось ";
	if (seconds >= 11 && seconds <= 19) {
		var seconds_text = " секунд";
	} else if (seconds % 10 == 2 || seconds % 10 == 3 || seconds % 10 == 4) {
		seconds_text = " секунды";
	} else if (seconds % 10 == 1) {
		seconds_text = " секунда";
		left = "осталась ";
	} else {
		seconds_text = " секунд";
	}
	paragraph.innerText = "До " + date_year + " года " + left + seconds + seconds_text;
}, 1);